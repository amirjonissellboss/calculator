import './App.css';
import React, {useState} from "react";
import axios from 'axios';
import ReactDOM from "react-dom";
import "./styles.css";


function App() {

    const [operations, setOperations] = useState(null);


    const fetchData = async () => {
        const response = await axios.get(
            "http://localhost:8080/calculator/service/operations"
        );

        setOperations(response.data);
    };

    const [results, setResults] = useState('');

    const performOperationAndFetchResult = async () => {

        const response = await     axios
            .get("http://localhost:8080/calculator/service/" + operation.toLowerCase(), {
                params: {
                    num1: input,
                    num2: input2
                }
            })

        setResults(response.data);
    };

    const [input, setInput] = useState(0);
    const [input2, setInput2] = useState(0);
   const [operation, setOperation] = useState('');


    return (


        <div className="App">
            <h1>Calculator</h1>
            <br/>
            <div>
                <label htmlFor="num1">Number 1: </label>
                    <input type="string" id="num1" name="num1" value={input} onInput={e => setInput(e.target.value)}/>
            </div>
            <br/>
            <div>
                <label htmlFor="num2">Number 2: </label>
                <input type="string" id="num2" name="num2"value={input2} onInput={e => setInput2(e.target.value)}/>
            </div>
            <br/>
            <div>
                <label htmlFor="results"> Result:     </label>
                <input type="string" id="result" name="num2"value={results}/>
            </div>
            
            <div onChange={e => setOperation(e.target.value)}>
                <input type="radio" value="addition" name="operation" /> Addition
                <br/>
                <input type="radio" value="subtraction" name="operation" /> Subtraction
                <br/>
                <input type="radio" value="multiplication" name="operation" /> Multiplication
                <br/>
                <input type="radio" value="division" name="operation" /> Division
            </div>

           <br/>
            <div>
                <button className="operation-button" onClick={performOperationAndFetchResult}>
                    Perform Operation
                </button>
                <br/>
            </div>

            <div>
                <button className="fetch-button" onClick={fetchData}>
                    Fetch Operations
                </button>
                <br/>
            </div>

            <div className="operations">
                {operations &&
                    operations.map((operation, index) => {
                        return (
                            <div className="operation" key={index}>
                                <h3>Operation {index + 1}</h3>

                                <div className="details">
                                    <p>{operation.num1} {operation.operationType
                                        .replaceAll('ADDITION', '+ ')
                                        .replaceAll('SUBTRACTION', '- ')
                                        .replaceAll('MULTIPLICATION', '* ')
                                        .replaceAll('DIVISION', '/ ')}
                                        {operation.num2} = {operation.result} </p>
                                </div>
                            </div>
                        );
                    })}
            </div>
        </div>
    );
}

export default class Calculator extends React.Component {
    state = {
        operations: []
    }

    componentDidMount() {
        const rootElement = document.getElementById("root");
        ReactDOM.render(<App/>, rootElement);
    }

    render() {
        const {totalReactPackages: totalOperations} = this.state;

        return (
            <div className="card text-center m-3">
                <h5 className="card-header">Simple GET Requests</h5>
                <div className="card-body">
                    Total operations: {totalOperations}
                </div>
            </div>
        );
    }


}

export {Calculator};
