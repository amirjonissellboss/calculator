package se.tele2.calculator.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import se.tele2.calculator.configuration.JdbcConfiguration;
import se.tele2.calculator.configuration.JdbcTestConfiguration;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;

import java.util.List;

@Transactional
@DirtiesContext
@SpringBootTest(classes = {
        JdbcConfiguration.class,
        JdbcOperationRepository.class,
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,})
@ExtendWith(SpringExtension.class)
class JdbcOperationRepositoryTest {

    @Autowired
    OperationRepository operationRepository;

    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;


    @BeforeEach
    void tearDown() {
        jdbcTemplate.getJdbcOperations().execute("TRUNCATE table operation");
    }

    @Test
    void saveOperation() {

        Operation operation = persistOperation(1, 2, OperationType.ADDITION, 3);

        Assertions.assertNotNull(operation);

    }


    @Test
    void getOperations() {
        Operation operation1 = persistOperation(1, 2, OperationType.ADDITION, 3);
        Operation operation2 = persistOperation(5, 2, OperationType.SUBTRACTION, 3);

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(2, operations.size());
        Assertions.assertTrue(operations.contains(operation1));
        Assertions.assertTrue(operations.contains(operation2));
    }

    @Test
    void isOperationExists() {
        Operation operation1 = persistOperation(2, 2, OperationType.MULTIPLICATION, 4);

        Operation operationExists = operationRepository.isOperationExists(operation1);

        Assertions.assertNotNull(operationExists);

    }

    private Operation persistOperation(double num1, double num2, OperationType operationType, double result) {
        Operation operation = new Operation();
        operation.setNum1(num1);
        operation.setNum2(num2);
        operation.setOperationType(operationType);
        operation.setResult(result);

        return operationRepository.saveOperation(operation);

    }
}