package se.tele2.calculator.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import se.tele2.calculator.repository.OperationRepository;

import java.sql.Types;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@DirtiesContext
public class GetOperationTest {

    @Autowired
    private
    MockMvc mockMvc;

    @Autowired
    OperationRepository operationRepository;

    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    private
    NamedParameterJdbcTemplate jdbcTemplate;

    @Test
    public void getPerformOperationAdditionOperationNotExists() throws Exception {
        OperationType addition = OperationType.ADDITION;

        mockMvc.perform(get("/calculator/service/" + addition.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(4, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationSubtractionOperationNotExists() throws Exception {
        OperationType sub = OperationType.SUBTRACTION;

        mockMvc.perform(get("/calculator/service/" + sub.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(0, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationMultiplicationOperationNotExists() throws Exception {
        OperationType multiplication = OperationType.MULTIPLICATION;

        mockMvc.perform(get("/calculator/service/" + multiplication.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(4, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationDivisionOperationNotExists() throws Exception {
        OperationType division = OperationType.DIVISION;

        mockMvc.perform(get("/calculator/service/" + division.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(1, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationAdditionAlreadyExistsShouldReturnExisting() throws Exception {
        OperationType addition = OperationType.ADDITION;

        Operation operation = new Operation(2, 2, 4.0, addition);
        persist(operation);

        mockMvc.perform(get("/calculator/service/" + addition.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(4, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationSubtractionAlreadyExistsShouldReturnExisting() throws Exception {
        OperationType sub = OperationType.SUBTRACTION;

        Operation operation = new Operation(2, 2, 0.0, sub);
        persist(operation);

        mockMvc.perform(get("/calculator/service/" + sub.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(0, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationMultiplicationAlreadyExistsShouldReturnExisting() throws Exception {
        OperationType multiplication = OperationType.MULTIPLICATION;

        Operation operation = new Operation(2, 2, 4.0, multiplication);
        persist(operation);

        mockMvc.perform(get("/calculator/service/" + multiplication.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(4, operations.get(0).getResult());

    }

    @Test
    public void getPerformOperationDivisionAlreadyExistsShouldReturnExisting() throws Exception {
        OperationType division = OperationType.DIVISION;

        Operation operation = new Operation(2, 2, 1.0, division);
        persist(operation);

        mockMvc.perform(get("/calculator/service/" + division.name().toLowerCase() + "?num1=2&num2=2"))
                .andExpect(status().isOk());

        List<Operation> operations = operationRepository.getOperations();
        Assertions.assertEquals(1, operations.size());
        Assertions.assertEquals(1, operations.get(0).getResult());

    }
    @Test
    public void getOperations() throws Exception {
        OperationType addition = OperationType.ADDITION;
        Operation additionOp = new Operation(2, 2, 4.0, addition);

        OperationType subtraction = OperationType.SUBTRACTION;
        Operation subtractionOp = new Operation(2, 2, 0.0, subtraction);

        OperationType multiplication = OperationType.MULTIPLICATION;
        Operation multiplicationOp = new Operation(2, 2, 4.0, multiplication);

        OperationType division = OperationType.DIVISION;
        Operation divisionOp = new Operation(2, 2, 1.0, division);

        List.of(additionOp, subtractionOp, multiplicationOp, divisionOp).forEach(this::persist);

        mockMvc.perform(get("/calculator/service/operations"))
                .andExpect(status().isOk())
                .andReturn();

    }

    void persist(Operation operation) {

        MapSqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("operationId", UUID.randomUUID(), Types.OTHER)
                .addValue("num1", operation.getNum1(), Types.DOUBLE)
                .addValue("num2", operation.getNum2(), Types.DOUBLE)
                .addValue("operationtype", operation.getOperationType().name())
                .addValue("result", operation.getResult());


        String sql = "INSERT INTO operation " +
                "(operationId, num1, num2, result, operationtype)" +
                " VALUES " +
                "(:operationId, :num1, :num2, :result, :operationtype)";

        jdbcTemplate.update(sql, parameterSource);
    }


}
