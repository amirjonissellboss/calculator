package se.tele2.calculator.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import se.tele2.calculator.exception.CalculatorException;
import se.tele2.calculator.service.CalculatorService;
import se.tele2.calculator.service.OperationService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OperationController.class})
@DirtiesContext
public class OperationControllerTest {
    @MockBean
    private OperationService mockOperationService;

    @MockBean
    private CalculatorService mockCalculatorService;

    private OperationController controller;

    @BeforeEach
    void setup() {
        controller = new OperationController(mockCalculatorService, mockOperationService);
    }

    @Test
    public void performOperationAddition() throws CalculatorException {

        double num1 = 1;
        double num2 = 1;
        double expected = 2.0d;
        Operation operationWithResult = new Operation(num1, num2, expected, OperationType.ADDITION);

        Operation operationWithoutResult = new Operation(num1, num2, OperationType.ADDITION);

        Mockito.when(mockOperationService.processOperation(any())).thenReturn(operationWithResult);

        double response = controller.performOperationAddition(num1, num2);

        assertEquals(expected, response);
        verify(mockOperationService, times(1)).processOperation(eq(operationWithoutResult));
        verifyNoMoreInteractions(mockOperationService);
    }

    @Test
    public void performOperationSubtraction() throws CalculatorException {

        double num1 = 1;
        double num2 = 1;
        double expected = 0;
        Operation operationWithResult = new Operation(num1, num2, expected, OperationType.SUBTRACTION);

        Operation operationWithoutResult = new Operation(num1, num2, OperationType.SUBTRACTION);

        Mockito.when(mockOperationService.processOperation(any())).thenReturn(operationWithResult);

        double response = controller.performOperationSubtraction(num1, num2);

        assertEquals(expected, response);
        verify(mockOperationService, times(1)).processOperation(eq(operationWithoutResult));
        verifyNoMoreInteractions(mockOperationService);
    }

    @Test
    public void performOperationMultiplication() throws CalculatorException {

        double num1 = 1;
        double num2 = 1;
        double expected = 1;
        Operation operationWithResult = new Operation(num1, num2, expected, OperationType.MULTIPLICATION);

        Operation operationWithoutResult = new Operation(num1, num2, OperationType.MULTIPLICATION);

        Mockito.when(mockOperationService.processOperation(any())).thenReturn(operationWithResult);

        double response = controller.performOperationMultiplication(num1, num2);

        assertEquals(expected, response);
        verify(mockOperationService, times(1)).processOperation(eq(operationWithoutResult));
        verifyNoMoreInteractions(mockOperationService);
    }

    @Test
    public void performOperationDivision() throws CalculatorException {

        double num1 = 1;
        double num2 = 1;
        double expected = 1;
        Operation operationWithResult = new Operation(num1, num2, expected, OperationType.DIVISION);

        Operation operationWithoutResult = new Operation(num1, num2, OperationType.DIVISION);

        Mockito.when(mockOperationService.processOperation(any())).thenReturn(operationWithResult);

        double response = controller.performOperationDivision(num1, num2);

        assertEquals(expected, response);
        verify(mockOperationService, times(1)).processOperation(eq(operationWithoutResult));
        verifyNoMoreInteractions(mockOperationService);
    }


    @Test
    public void getAllOperations() {

        double num1 = 1;
        double num2 = 1;
        double expected = 2.0f;

        Operation operation = new Operation(num1, num2, expected, OperationType.ADDITION);

        Mockito.when(mockOperationService.getOperations()).thenReturn(List.of(operation));

        List<Operation> response = controller.getOperations();

        assertTrue(response.contains(operation));
        verify(mockOperationService, times(1)).getOperations();
        verifyNoMoreInteractions(mockOperationService);
        verifyNoInteractions(mockCalculatorService);
    }


}
