package se.tele2.calculator.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@Configuration
public class JdbcTestConfiguration {

/*    Logger log = LoggerFactory.getLogger(Resource.class);

    private String url = "jdbc:h2:mem:~/db;DB_CLOSE_DELAY=-1";
    private String driver = "org.h2.Driver";
    private String username = "targaryen";
    private String password = "targaryen";

    private String initScript = "db/create.sql";

    @Bean
    public DataSource dataSource() {
        log.info("# Skapar datasource för H2");
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        Resource initDatabase = new ClassPathResource(initScript);
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initDatabase);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);

        log.info("{}", dataSource);
        return dataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        this.log.info("Skapar NamedJdbcTemplate med: [dataSource: {}]", dataSource);
        return new NamedParameterJdbcTemplate(dataSource);
    }*/

}
