package se.tele2.calculator.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DefaultCalculatorServiceTest {

    private CalculatorService calculatorService;

    @BeforeEach
     void init() {
        calculatorService = new DefaultCalculatorService();
    }

    @Test
    void calculateAddition() {
        double result = calculatorService.calculateAddition(1, 2);
        Assertions.assertEquals(3, result);
    }

    @Test
    void calculateSubtraction() {
        double result = calculatorService.calculateSubtraction(5, 2);
        Assertions.assertEquals(3, result);
    }

    @Test
    void calculateDivision() {
        double result = calculatorService.calculateDivision(10, 2);
        Assertions.assertEquals(5, result);
    }

    @Test
    void calculateMultiplication() {
        double result = calculatorService.calculateMultiplication(2, 2);
        Assertions.assertEquals(4, result);
    }
}