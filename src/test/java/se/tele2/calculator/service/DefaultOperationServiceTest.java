package se.tele2.calculator.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import se.tele2.calculator.exception.CalculatorException;
import se.tele2.calculator.repository.OperationRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

class DefaultOperationServiceTest {

    @Mock
    CalculatorService calculatorService;

    @Mock
    OperationRepository operationRepository;

    private OperationService operationService;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);

        operationService = new DefaultOperationService(calculatorService, operationRepository);
    }

    @Test
    void processOperationAdditionAlreadyExistsShouldSucceedAndNotCalculateNewResult() throws CalculatorException {

        Operation operation = new Operation(1, 1, 2.0, OperationType.ADDITION);
        Mockito.when(operationRepository.isOperationExists(any())).thenReturn(operation);

        operationService.processOperation(operation);

        Mockito.verifyNoInteractions(calculatorService);
        Mockito.verify(operationRepository, Mockito.times(1)).isOperationExists(eq(operation));

    }

    @Test
    void processOperationAdditionDoesNotExistShouldCalculateNewResult() throws CalculatorException {

        Operation operation = new Operation(2, 1, null, OperationType.SUBTRACTION);
        Mockito.when(operationRepository.isOperationExists(any())).thenReturn(null);

        operationService.processOperation(operation);

        Mockito.verify(calculatorService, Mockito.times(1)).calculateSubtraction(eq(2.0d), eq(1.0d));
        Mockito.verify(operationRepository, Mockito.times(1)).saveOperation(eq(operation));

    }
}