package se.tele2.calculator.repository;

import org.springframework.stereotype.Repository;
import se.tele2.calculator.domain.Operation;

import java.util.List;

@Repository
public interface OperationRepository {

    Operation saveOperation(Operation operation);

    List<Operation> getOperations();

    Operation isOperationExists(Operation operation);
}
