package se.tele2.calculator.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.repository.mapper.OperationRowMapper;

import java.util.List;
import java.util.UUID;

@Repository
public class JdbcOperationRepository implements OperationRepository {

    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Operation saveOperation(Operation operation) {
        String sql = "INSERT INTO operation " +
                "(operationId, num1, num2, result, operationtype)" +
                " VALUES " +
                "(:operationId, :num1, :num2, :result, :operationtype)";

        UUID uuid = UUID.randomUUID();
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("operationId", uuid)
                .addValue("num1", operation.getNum1())
                .addValue("num2", operation.getNum2())
                .addValue("result", operation.getResult())
                .addValue("operationtype", operation.getOperationType().name());

        jdbcTemplate.update(sql, parameters);

        return operation;
    }

    @Override
    public List<Operation> getOperations() {
        String sql = "SELECT * FROM operation";

        return jdbcTemplate.query(sql, new OperationRowMapper());
    }

    @Override
    public Operation isOperationExists(Operation operation) {

        try {
            String sql = "SELECT * FROM operation WHERE num1 = :num1 AND num2 = :num2 AND operationtype = :operationtype";

            MapSqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("num1", operation.getNum1())
                    .addValue("num2", operation.getNum2())
                    .addValue("operationtype", operation.getOperationType().name());

            return jdbcTemplate.queryForObject(sql, parameters, new OperationRowMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
