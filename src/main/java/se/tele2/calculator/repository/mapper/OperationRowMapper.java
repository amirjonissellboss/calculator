package se.tele2.calculator.repository.mapper;

import org.springframework.jdbc.core.RowMapper;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OperationRowMapper implements RowMapper<Operation> {

    @Override
    public Operation mapRow(ResultSet rs, int rowNum) throws SQLException {

        Operation operation = new Operation();
        operation.setNum1(rs.getDouble("num1"));
        operation.setNum2(rs.getDouble("num2"));
        operation.setOperationType(OperationType.valueOf(rs.getString("operationtype")));
        operation.setResult(rs.getDouble("result"));

        return operation;
    }

}
