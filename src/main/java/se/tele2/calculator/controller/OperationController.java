package se.tele2.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import se.tele2.calculator.exception.CalculatorException;
import se.tele2.calculator.service.CalculatorService;
import se.tele2.calculator.service.OperationService;

import java.util.List;

@RestController
@RequestMapping("/calculator/service")
public class OperationController {
    private final CalculatorService calculatorService;

    private final OperationService operationService;

    @Autowired
    public OperationController(CalculatorService calculatorService, OperationService operationService) {
        this.calculatorService = calculatorService;
        this.operationService = operationService;
    }

    @RequestMapping(value = "/addition", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public double performOperationAddition(@RequestParam double num1, @RequestParam double num2) throws CalculatorException {


        return getOperation(num1, num2, OperationType.ADDITION);
    }

    @RequestMapping(value = "/subtraction", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public double performOperationSubtraction(@RequestParam double num1, @RequestParam double num2) throws CalculatorException {


        return getOperation(num1, num2, OperationType.SUBTRACTION);
    }

    @RequestMapping(value = "/multiplication", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public double performOperationMultiplication(@RequestParam double num1, @RequestParam double num2) throws CalculatorException {


        return getOperation(num1, num2, OperationType.MULTIPLICATION);
    }

    @RequestMapping(value = "/division", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public double performOperationDivision(@RequestParam double num1, @RequestParam double num2) throws CalculatorException {


        return getOperation(num1, num2, OperationType.DIVISION);
    }

    @RequestMapping(value = "/operations", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public List<Operation> getOperations() {


        return getAllOperation();
    }


    private double getOperation(double num1, double num2, OperationType operationType) throws CalculatorException {

        Operation operation = new Operation(num1, num2, operationType);

       switch (operationType) {
           case ADDITION:
           case SUBTRACTION:
           case DIVISION:
           case MULTIPLICATION:
               return operationService.processOperation(operation).getResult();
           default:  throw new RuntimeException("Invalid Operation Type");
       }
    }

    private List<Operation> getAllOperation() {

        return operationService.getOperations();
    }
}
