package se.tele2.calculator.service;

import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.exception.CalculatorException;

import java.util.List;

public interface OperationService {

    Operation processOperation(Operation operation) throws CalculatorException;

    List<Operation> getOperations();

}
