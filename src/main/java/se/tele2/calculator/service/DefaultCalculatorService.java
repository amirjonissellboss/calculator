package se.tele2.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class DefaultCalculatorService implements CalculatorService {

    @Override
    public double calculateAddition(double num1, double num2) {
        return num1 + num2;
    }

    @Override
    public double calculateSubtraction(double num1, double num2) {
        return num1 - num2;
    }

    @Override
    public double calculateDivision(double num1, double num2) {
        return num1 / num2;
    }

    @Override
    public double calculateMultiplication(double num1, double num2) {
        return num1 * num2;
    }
}
