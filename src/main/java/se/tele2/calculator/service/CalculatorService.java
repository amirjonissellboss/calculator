package se.tele2.calculator.service;

public interface CalculatorService {

    double calculateAddition(double num1, double num2);

    double calculateSubtraction(double num1, double num2);

    double calculateDivision(double num1, double num2);

    double calculateMultiplication(double num1, double num2);

}
