package se.tele2.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.tele2.calculator.domain.Operation;
import se.tele2.calculator.enumeration.OperationType;
import se.tele2.calculator.exception.CalculatorException;
import se.tele2.calculator.repository.OperationRepository;

import java.util.List;

@Service
public class DefaultOperationService implements OperationService {

    private CalculatorService calculatorService;

    private OperationRepository operationRepository;

    @Autowired
    public DefaultOperationService(CalculatorService calculatorService, OperationRepository operationRepository) {
        this.calculatorService = calculatorService;
        this.operationRepository = operationRepository;
    }

    @Override
    public Operation processOperation(Operation operation) throws CalculatorException {

        Operation operationExists = operationRepository.isOperationExists(operation);

        if (operationExists != null) {
            return operationExists;
        } else {
            return calculateResult(operation);
        }

    }

    @Override
    public List<Operation> getOperations() {
        return operationRepository.getOperations();
    }

    private Operation calculateResult(Operation operation) throws CalculatorException {
        OperationType operationType = operation.getOperationType();

        double result;

        switch (operationType) {
            case ADDITION:
                result = calculatorService.calculateAddition(operation.getNum1(), operation.getNum2());
                break;
            case SUBTRACTION:
                result = calculatorService.calculateSubtraction(operation.getNum1(), operation.getNum2());
                break;
            case MULTIPLICATION:
                result = calculatorService.calculateMultiplication(operation.getNum1(), operation.getNum2());
                break;
            case DIVISION:
                result = calculatorService.calculateDivision(operation.getNum1(), operation.getNum2());
                break;
            default:
                throw new CalculatorException("Operation not supported.");
        }

        operation.setResult(result);
        operationRepository.saveOperation(operation);
        return operation;
    }
}
