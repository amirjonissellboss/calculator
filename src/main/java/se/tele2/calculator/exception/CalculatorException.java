package se.tele2.calculator.exception;

public class CalculatorException extends Exception{

    public CalculatorException(String message) {
        super(message);
    }
}
