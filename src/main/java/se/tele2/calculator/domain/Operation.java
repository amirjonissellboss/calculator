package se.tele2.calculator.domain;

import se.tele2.calculator.enumeration.OperationType;

import java.util.Objects;

public class Operation {

    private double num1;
    private double num2;
    private Double result;
    private OperationType operationType;

    public Operation() {
    }

    public Operation(double num1, double num2, Double result, OperationType operationType) {
        this.num1 = num1;
        this.num2 = num2;
        this.result = result;
        this.operationType = operationType;
    }

    public Operation(double num1, double num2, OperationType operationType) {
        this.num1 = num1;
        this.num2 = num2;
        this.operationType = operationType;
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Double.compare(operation.num1, num1) == 0 && Double.compare(operation.num2, num2) == 0 && Objects.equals(result, operation.result) && operationType == operation.operationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(num1, num2, result, operationType);
    }

    @Override
    public String toString() {
        return "Operation{" +
                "num1=" + num1 +
                ", num2=" + num2 +
                ", result=" + result +
                ", operationType=" + operationType +
                '}';
    }
}
