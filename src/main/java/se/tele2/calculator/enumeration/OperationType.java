package se.tele2.calculator.enumeration;

public enum OperationType {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION
}
